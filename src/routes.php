<?php
$routeAttr = config('laravel-sms.route', []);
unset($routeAttr['enable']);

$attributes = array_merge([
    'prefix' => 'laravel-sms',
], $routeAttr);


Route::group($attributes, function () {
    Route::get('info', 'SintaLaravel\Sms\Controllers\SmsController@getInfo');
    Route::post('verify-code', 'SintaLaravel\Sms\Controllers\SmsController@postSendCode');
    Route::post('voice-verify', 'SintaLaravel\Sms\Controllers\SmsController@postVoiceVerify');
});