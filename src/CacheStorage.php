<?php
namespace SintaLaravel\Sms;

use Illuminate\Support\Facades\Cache;

class CacheStorage
{
    protected static $lifetime = 120;

    public static function setMinutesOfLifeTime($time)
    {
        if (is_int($time) && $time > 0) {
            self::$lifetime = $time;
        }
    }

    public function set($key, $value)
    {
        Cache::put($key, $value, self::$lifetime);
    }
}