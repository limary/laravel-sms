<?php
namespace SintaLaravel\Sms\Contracts;

/***
 * 发送模板语音
 *
 * Interface TemplateVoice
 * @package SintaLaravel\Sms\Contracts
 */
interface TemplateVoice
{
    public function sendTemplateVoice($to, $tempId, array $tempData);
}