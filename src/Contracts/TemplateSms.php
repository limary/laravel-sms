<?php
namespace SintaLaravel\Sms\Contracts;

/**
 * 发送模板短信
 *
 * Interface TemplateSms
 * @package Sintasms\Sms\Contracts
 */
interface TemplateSms
{
    public function sendTemplateSms($to, $tempId, array $tempData);
}