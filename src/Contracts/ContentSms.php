<?php
namespace SintaLaravel\Sms\Contracts;

/**
 * 发送内容接口
 *
 * Interface ContentSms
 * @package SintaLaravel\Sms\Contracts
 */
interface ContentSms
{
    public function sendContentSms($to, $content);
}