<?php
namespace SintaLaravel\Sms\Contracts;


interface FileVoice
{
    public function sendFileVoice($to, $fileId);
}