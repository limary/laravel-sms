<?php
namespace SintaLaravel\Sms\Contracts;


interface ContentVoice
{
    public function sendContentVoice($to, $content);
}