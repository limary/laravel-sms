<?php
namespace SintaLaravel\Sms\Storage;


interface IStorage
{
    public function set($key, $value);
    public function get($key, $default);
    public function forget($key);
}