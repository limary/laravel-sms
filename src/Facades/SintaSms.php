<?php
namespace SintaLaravel\Sms\Facades;


use Illuminate\Support\Facades\Facade;

class SintaSms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SintaLaravel\\Sms\\SintaSms';
    }
}