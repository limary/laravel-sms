<?php
namespace SintaLaravel\Sms;

use Closure;

class Util
{

    /**
     * 根据模版和数据合成字符串
     *
     * @param string        $template
     * @param array         $data
     * @param \Closure|null $onError
     *
     * @return string
     */
    public static function vsprintf($template, array $data, \Closure $onError = null)
    {
        if (!is_string($template)) {
            return '';
        }
        if ($template && !(empty($data))) {
            try {
                $template = vsprintf($template, $data);
            } catch (\Exception $e) {
                if ($onError) {
                    call_user_func($onError, $e);
                }
            }
        }
        return $template;
    }


    /**
     * 获取路径中的path部分
     *
     * @param string        $url
     * @param \Closure|null $onError
     *
     * @return string
     */
    public static function pathOfUrl($url, \Closure $onError = null)
    {
        $path = '';
        if (!is_string($url)) {
            return $path;
        }
        try {
            $parsed = parse_url($url);
            $path = $parsed['path'];
        } catch (\Exception $e) {
            if ($onError) {
                call_user_func($onError, $e);
            }
        }
        return $path;
    }


    /**
     * 从数组中获取值，并删除
     *
     * @param array $options
     * @param $key
     * @return mixed|null
     */
    public static function pullFromArray(array &$options, $key)
    {
        $value = null;
        if (!isset($options[$key])) {
            return $value;
        }
        $value = $options[$key];
        unset($options[$key]);

        return $value;
    }

    public static function operateArray(array &$array, $key, $value = null, $default = null,
                        Closure $setter = null, $override = false, $willOverride = null, $isSet = false){
        if (!$isSet && ($key === null || is_string($key) || is_int($key)) && $value === null) {
            return $key === null ? $array :
                (isset($array[$key]) ? $array[$key] : $default);
        }
        if ($override) {
            if (is_callable($willOverride)) {
                call_user_func_array($willOverride, [$array]);
            }
            $array = [];
        }
        if (is_array($key) || is_object($key)) {
            foreach ($key as $k => $v) {
                self::operateArray($array, $k, $v, $default, $setter, false, null, true);
            }
        } elseif (is_callable($setter)) {
            call_user_func_array($setter, [$key, $value]);
        } else {
            $array[$key] = $value;
        }

        return $array;
    }


    /**
     * 格式手机号
     *
     * @param $target
     * @return array
     */
    public static function formatMobiles($target)
    {
        if (!is_array($target)) {
            return [$target];
        }
        $list = [];
        $nation = $number = null;
        $count = count($target);
        if ($count === 2) {
            $firstItem = $target[0];
            if (is_int($firstItem) && $firstItem > 0 && $firstItem <= 9999) {
                $nation = $firstItem;
                $number = $target[1];
            }
            if (is_string($firstItem) && strlen($firstItem = trim($firstItem)) <= 4) {
                $nation = $firstItem;
                $number = $target[1];
            }
        }
        if (!is_null($nation)) {
            return [compact('nation', 'number')];
        }
        foreach ($target as $childTarget) {
            $childList = self::formatMobiles($childTarget);
            foreach ($childList as $childListItem) {
                array_push($list, $childListItem);
            }
        }

        return $list;
    }
}