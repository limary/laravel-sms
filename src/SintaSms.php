<?php
namespace SintaLaravel\Sms;

use SintaLaravel\Sms\Agents\ProxyAgent;
use SintaLaravel\Sms\Exception\LaravelSmsException;
use SintaLaravel\Sms\Task\Driver;
use SintaLaravel\Sms\Task\Task;

class SintaSms
{
    const TYPE_SMS = 1;
    const TYPE_VOICE = 2;

    //任务实例
    protected static $task = null;
    //客户设置
    protected static $agentsConfig = [];
    //客户端实例
    protected static $agents = [];

    protected static $enableQueue = false;

    protected static $howToUseQueue = null;

    protected static $scheme = [];

    protected $firstAgent = null;

    /**
     * Whether pushed to the queue system.
     *
     * @var bool
     */
    protected $pushedToQueue = false;

    /**
     * @var array 短信数据
     */
    protected $smsData = [
        'type'      => self::TYPE_SMS,
        'to'        => null,
        'templates' => [],
        'data'      => [],
        'content'   => null,
        'code'      => null,
        'files'     => [],
        'params'    => [],
    ];

    public function __construct($autoBoot = true)
    {
        if ($autoBoot) {
            self::bootTask();
        }
    }

    public static function bootTask()
    {
        if (!self::isTaskBooted()) {
            self::configure();
            foreach (self::scheme() as $name => $scheme) {
                self::registerDriver($name, $scheme);
            }
        }
    }

    protected static function isTaskBooted()
    {
        return !empty(self::getTask()->drivers);
    }


    public static function getTask()
    {
        if (empty(self::$task)) {
            self::$task = new Task();
        }

        return self::$task;
    }

    protected static function configure()
    {
        $config = [];
        if (!count(self::scheme())) {
            self::initScheme($config);
        }
        $diff = array_diff_key(self::scheme(), self::$agentsConfig);
        self::initAgentsConfig(array_keys($diff), $config);
        if (!count(self::scheme())) {
            throw new LaravelSmsException('Expected at least one agent in scheme.');
        }
    }

    protected static function initAgentsConfig(array $agents, array &$config)
    {
        if (empty($agents)) {
            return;
        }
        $config = empty($config) ? include __DIR__ . '/../config/laravel-sms.php' : $config;
        $agentsConfig = isset($config['agents']) ? $config['agents'] : [];
        foreach ($agents as $name) {
            $agentConfig = isset($agentsConfig[$name]) ? $agentsConfig[$name] : [];
            self::config($name, $agentConfig);
        }
    }

    protected static function initScheme(array &$config)
    {
        $config = empty($config) ? include __DIR__ . '/../config/laravel-sms.php' : $config;
        $scheme = isset($config['scheme']) ? $config['scheme'] : [];
        self::scheme($scheme);
    }



    /**
     * 注册驱动器
     *
     * @param $name
     * @param $scheme
     */
    protected static function registerDriver($name, $scheme)
    {
        // parse higher-order scheme
        $settings = [];
        if (is_array($scheme)) {
            $settings = self::parseScheme($scheme);
            $scheme = $settings['scheme'];
        }

        // register
        self::getTask()->driver("$name $scheme")->work(function (Driver $driver) use ($settings) {

            $agent = self::getAgent($driver->name, $settings);
            extract($driver->getTaskData());
            $template = isset($templates[$driver->name]) ? $templates[$driver->name] : null;
            $file = isset($files[$driver->name]) ? $files[$driver->name] : null;
            $params = isset($params[$driver->name]) ? $params[$driver->name] : [];


            if ($type === self::TYPE_VOICE) {
                $agent->sendVoice($to, $content, $template, $data, $code, $file, $params);
            } elseif ($type === self::TYPE_SMS) {
                $agent->sendSms($to, $content, $template, $data, $params);
            }
            $result = $agent->result();
            if ($result['success']) {
                $driver->success();
            }
            unset($result['success']);

            return $result;
        });
    }



    public static function make($agentName = null, $tempId = null)
    {
        $sms = new self();
        $sms->type(self::TYPE_SMS);
        if (is_array($agentName)) {
            $sms->template($agentName);
        } elseif ($agentName && is_string($agentName)) {
            if ($tempId === null) {
                $sms->content($agentName);
            } elseif (is_string($tempId) || is_int($tempId)) {
                $sms->template($agentName, "$tempId");
            }
        }

        return $sms;
    }



    /**
     * 处理设置
     *
     * @param null $name
     * @param null $config
     * @param bool $override
     * @return mixed
     */
    public static function config($name = null, $config = null, $override = false)
    {
        $overrideAll = (is_array($name) && is_bool($config)) ? $config : false;

        return Util::operateArray(self::$agentsConfig, $name, $config, [], function ($name, array $config) use ($override) {
            self::modifyConfig($name, $config, $override);
        }, $overrideAll, function (array $origin) {
            foreach (array_keys($origin) as $name) {
                if (self::hasAgent("$name")) {
                    self::getAgent("$name")->config([], true);
                }
            }
        });
    }

    protected static function modifyConfig($name, array $config, $override = false)
    {
        self::validateAgentName($name);
        if (!isset(self::$agentsConfig[$name])) {
            self::$agentsConfig[$name] = [];
        }
        $target = &self::$agentsConfig[$name];
        if ($override) {
            $target = $config;
        } else {
            $target = array_merge($target, $config);
        }
        if (self::hasAgent($name)) {
            self::getAgent($name)->config($target);
        }
    }


    protected static function validateAgentName($name)
    {
        if (empty($name) || !is_string($name) || preg_match('/^[0-9]+$/', $name)) {
            throw new LaravelSmsException('Expected the name of agent to be a string which except the digital string.');
        }
    }


    public static function hasAgent($name)
    {
        return isset(self::$agents[$name]);
    }

    /**
     * @param array $options
     * @return array
     */
    protected static function parseScheme(array $options)
    {
        $weight = Util::pullFromArray($options, 'weight');
        $backup = Util::pullFromArray($options, 'backup') ? 'backup' : '';
        $props = array_filter(array_values($options), function ($prop) {
            return is_numeric($prop) || is_string($prop);
        });

        $options['scheme'] = implode(' ', $props) . " $weight $backup";

        return $options;
    }

    /**
     * 获取客户端处理对象
     *
     * @param $name
     * @param array $options
     * @throws LaravelSmsException
     */
    public static function getAgent($name,array $options = [])
    {
        if(!self::hasAgent($name)){
            $scheme = self::scheme($name);
            $config = self::config($name);
            if (is_array($scheme) && empty($options)) {
                $options = self::parseScheme($scheme);
            }
            if (isset($options['scheme'])) {
                unset($options['scheme']);
            }
            $className = "SintaLaravel\\Sms\\Agents\\{$name}Agent";
            if (isset($options['agentClass'])) {
                $className = $options['agentClass'];
                unset($options['agentClass']);
            }
            if (!empty($options)) {
                self::$agents[$name] = new ProxyAgent($config, $options);
            } elseif (class_exists($className)) {
                self::$agents[$name] = new $className($config);
            } else {
                throw new LaravelSmsException("Not support agent `$name`.");
            }
        }
        return self::$agents[$name];
    }

    public function template($name, $tempId = null)
    {
        Util::operateArray($this->smsData['templates'], $name, $tempId);

        return $this;
    }



    public static function scheme($name = null, $scheme = null, $override = false)
    {
        if (is_array($name) && is_bool($scheme)) {
            $override = $scheme;
        }

        return Util::operateArray(self::$scheme, $name, $scheme, null, function ($key, $value) {
            if (is_string($key)) {
                self::modifyScheme($key, is_array($value) ? $value : "$value");
            } elseif (is_int($key)) {
                self::modifyScheme($value, '');
            }
        }, $override, function (array $origin) {
            if (self::isTaskBooted()) {
                foreach (array_keys($origin) as $name) {
                    self::getTask()->removeDriver($name);
                }
            }
        });
    }

    protected static function modifyScheme($name, $scheme)
    {
        self::validateAgentName($name);
        self::$scheme[$name] = $scheme;
        if (self::isTaskBooted()) {
            $driver = self::getTask()->getDriver($name);
            if ($driver) {
                if (is_array($scheme)) {
                    $higherOrderScheme = self::parseScheme($scheme);
                    $scheme = $higherOrderScheme['scheme'];
                }
                $driver->reset($scheme);
            } else {
                self::registerDriver($name, $scheme);
            }
        }
    }

    public function type($type)
    {
        if ($type !== self::TYPE_SMS && $type !== self::TYPE_VOICE) {
            throw new LaravelSmsException('Expected the parameter equals to `Sms::TYPE_SMS` or `Sms::TYPE_VOICE`.');
        }
        $this->smsData['type'] = $type;

        return $this;
    }

    public function to($mobile)
    {
        if (is_string($mobile)) {
            $mobile = trim($mobile);
        }
        $this->smsData['to'] = $mobile;

        return $this;
    }

    public function content($content)
    {
        $this->smsData['content'] = trim((string) $content);

        return $this;
    }

    public function data($key, $value = null)
    {
        Util::operateArray($this->smsData['data'], $key, $value);

        return $this;
    }

    public function send($immediately = false)
    {
        if (!self::$enableQueue || $this->pushedToQueue) {
            $immediately = true;
        }
        if ($immediately) {
            return self::$task->data($this->all())->run($this->firstAgent);
        }

        return $this->push();
    }

    public function push()
    {
        if (!is_callable(self::$howToUseQueue)) {
            throw new LaravelSmsException('Expected define how to use the queue system by methods `queue`.');
        }
        try {
            $this->pushedToQueue = true;

            return call_user_func_array(self::$howToUseQueue, [$this, $this->all()]);
        } catch (\Exception $e) {
            $this->pushedToQueue = false;
            throw $e;
        }
    }

    public function all($key = null)
    {
        if ($key !== null) {
            return isset($this->smsData[$key]) ? $this->smsData[$key] : null;
        }

        return $this->smsData;
    }
}