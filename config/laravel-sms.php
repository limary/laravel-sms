<?php
return [
    /*
   |--------------------------------------------------------------------------
   | 内置路由
   |--------------------------------------------------------------------------
   |
   | 如果是 web 应用建议 middleware 为 ['web', ...]
   | 如果是 api 应用建议 middleware 为 ['api', ...]
   |
   */
    'route' => [
        'enable' => true,
        'prefix' => 'v1/verifycodes',
        'middleware' => ['api']
    ],
    /*
    |--------------------------------------------------------------------------
    | 请求间隔
    |--------------------------------------------------------------------------
    |
    | 单位：秒
    |
    */
    'interval' => 60,
    /*
    |--------------------------------------------------------------------------
    | 数据验证管理
    |--------------------------------------------------------------------------
    |
    | 设置从客户端传来的需要验证的数据字段(`field`)
    |
    | - isMobile    是否为手机号字段
    | - enable      是否开启验证
    | - default     默认静态验证规则
    | - staticRules 静态验证规则
    |
    */
    'validation' => [
        'mobile' => [
            'isMobile'    => true,
            'enable'      => true,
            'default'     => 'mobile_required',
            'staticRules' => [
                'mobile_required'     => 'required|zh_mobile',
                'check_mobile_unique' => 'required|zh_mobile|unique:users,mobile',
                'check_mobile_exists' => 'required|zh_mobile|exists:users',
            ],
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | 验证码管理
    |--------------------------------------------------------------------------
    |
    | - length        验证码长度
    | - validMinutes  验证码有效时间长度，单位为分钟
    | - repeatIfValid 如果原验证码还有效，是否重复使用原验证码
    | - maxAttempts   验证码最大尝试验证次数，超过该数值验证码自动失效，0或负数则不启用
    |
    */
    'code' => [
        'length'        => 5,
        'validMinutes'  => 5,
        'repeatIfValid' => false,
        'maxAttempts'   => 0,
    ],

    /*
    |--------------------------------------------------------------------------
    | 验证码短信模版
    |--------------------------------------------------------------------------
    |
    | 每项数据的值可以为以下三种之一:
    |
    | - 字符串/数字
    |   如: 'YunTongXun' => '短信模版id'
    |
    | - 数组
    |   如: 'Alidayu' => ['短信模版id', '语音模版id'],
    |
    | - 匿名函数
    |   如: 'YunTongXun' => function ($input, $type) {
    |           return $input['isRegister'] ? 'registerTempId' : 'commonId';
    |       }
    |
    | 如需缓存配置，则需使用 `SintaLaravel\Sms\SmsManger::closure($closure)` 方法对匿名函数进行配置
    |
    */
    'templates' => [
        'Aliyun'    => ['SMS_94530040'],
    ],

    /*
    |--------------------------------------------------------------------------
    | 模版数据管理
    |--------------------------------------------------------------------------
    |
    | 每项数据的值可以为以下两种之一:
    |
    | - 基本数据类型
    |   如: 'minutes' => 5
    |
    | - 匿名函数（如果该函数不返回任何值，即表示不使用该项数据）
    |   如: 'serialNumber' => function ($code, $minutes, $input, $type) {
    |           return $input['serialNumber'];
    |       }
    |   如: 'hello' => function ($code, $minutes, $input, $type) {
    |           //不返回任何值，那么hello将会从模版数据中移除 :)
    |       }
    |
    | 如需缓存配置，则需使用 `SintaLaravel\Sms\SmsManger::closure($closure)` 方法对匿名函数进行配置
    |
    */
    'data' => [
        'code' => function ($code) {
            return $code;
        },
        'minutes' => function ($code, $minutes) {
            return $minutes;
        },
    ],
    /*
    |--------------------------------------------------------------------------
    | 存储系统配置
    |--------------------------------------------------------------------------
    |
    | driver:
    | 存储方式,是一个实现了'Toplan\Sms\Storage'接口的类的类名,
    | 内置可选的值有'Toplan\Sms\SessionStorage'和'Toplan\Sms\CacheStorage',
    | 如果不填写driver,那么系统会自动根据内置路由的属性(route)中middleware的配置值选择存储器driver:
    | - 如果中间件含有'web',会选择使用'SintaLaravel\Sms\SessionStorage'
    | - 如果中间件含有'api',会选择使用'SintaLaravel\Sms\CacheStorage'
    |
    | prefix:
    | 存储key的prefix
    |
    | 内置driver的个性化配置:
    | - 在laravel项目的'config/session.php'文件中可以对'Toplan\Sms\SessionStorage'进行更多个性化设置
    | - 在laravel项目的'config/cache.php'文件中可以对'Toplan\Sms\CacheStorage'进行更多个性化设置
    |
    */
    'storage' => [
        'driver' => '',
        'prefix' => 'laravel_sms',
    ],
    /*
   |--------------------------------------------------------------------------
   | 是否数据库记录发送日志
   |--------------------------------------------------------------------------
   |
   | 若需开启此功能,需要先生成一个内置的'laravel_sms'表
   | 运行'php artisan migrate'命令可以自动生成
   |
   */
    'dbLogs' => false,
    /*
    |--------------------------------------------------------------------------
    | 队列任务
    |--------------------------------------------------------------------------
    |
    */
    'queueJob' => 'SintaLaravel\Sms\SendReminderSms',
    /*
   |--------------------------------------------------------------------------
   | 验证码模块提示信息
   |--------------------------------------------------------------------------
   |
   */
    'notifies' => [
        // 频繁请求无效的提示
        'request_invalid' => '请求无效，请在%s秒后重试',
        // 验证码短信发送失败的提示
        'sms_sent_failure' => '短信验证码发送失败，请稍后重试',
        // 语音验证码发送发送成功的提示
        'voice_sent_failure' => '语音验证码请求失败，请稍后重试',
        // 验证码短信发送成功的提示
        'sms_sent_success' => '短信验证码发送成功，请注意查收',
        // 语音验证码发送发送成功的提示
        'voice_sent_success' => '语音验证码发送成功，请注意接听',
    ],
    /**
     *
     */
    'scheme' => [
        'Aliyun',
    ],
    /**
     * 客户端设置
     */
    'agents' => [
        /*
         * -----------------------------------
         * 云片代理器
         * -----------------------------------
         * website:http://www.yunpian.com
         * 支持内容短信
         */
        'YunPian' => [
            //用户唯一标识，必须
            'apikey' => 'your_api_key',
        ],
        /*
         * -----------------------------------
         * 云通讯代理器
         * -----------------------------------
         * website:http://www.yuntongxun.com/
         * 支持模板短信
         */
        'YunTongXun' => [
            //主帐号
            'accountSid'    => 'your_account_sid',
            //主帐号令牌
            'accountToken'  => 'your_account_token',
            //应用Id
            'appId'         => 'your_app_id',
            //请求地址(不加协议前缀)
            'serverIP'      => 'app.cloopen.com',
            //请求端口
            'serverPort'    => '8883',
            //被叫号显
            'displayNum'    => null,
            //语音验证码播放次数
            'playTimes'     => 3,
        ],
        /*
         * -----------------------------------
         * SubMail
         * -----------------------------------
         * website:http://submail.cn/
         * 支持模板短信
         */
        'SubMail' => [
            'appid'     => 'your_app_id',
            'signature' => 'your app key',
        ],
        /*
        * -----------------------------------
        * luosimao
        * -----------------------------------
        * website:http://ucpaas.com
        * 支持内容短信
        */
        'Luosimao' => [
            'apikey'        => 'your_api_key',
            'voiceApikey'   => 'your_voice_api_key',
        ],
        /*
        * -----------------------------------
        * ucpaas
        * -----------------------------------
        * website:http://ucpaas.com
        * 支持模板短信
        */
        'Ucpaas' => [
            //主帐号,对应开官网发者主账号下的 ACCOUNT SID
            'accountSid'    => 'your_account_sid',
            //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
            'accountToken'  => 'your_account_token',
            //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
            //在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
            'appId'         => 'your_app_id',
        ],
        /*
         * -----------------------------------
         * JuHe
         * 聚合数据
         * -----------------------------------
         * website:https://www.juhe.cn
         * support template sms.
         */
        'JuHe' => [
            //应用App Key
            'key'   => 'your_key',
            //语音验证码播放次数
            'times' => 3,
        ],
        /*
         * -----------------------------------
         * Alidayu
         * 阿里大鱼代理器
         * -----------------------------------
         * website:http://www.alidayu.com
         * 支持模板短信
         */
        'Alidayu' => [
            //请求地址
            'sendUrl'           => 'http://gw.api.taobao.com/router/rest',
            //淘宝开放平台中，对应阿里大鱼短信应用的App Key
            'appKey'            => 'your_app_key',
            //淘宝开放平台中，对应阿里大鱼短信应用的App Secret
            'secretKey'         => 'your_secret_key',
            //短信签名，传入的短信签名必须是在阿里大鱼“管理中心-短信签名管理”中的可用签名
            'smsFreeSignName'   => 'your_sms_free_sign_name',
            //被叫号显(用于语音通知)，传入的显示号码必须是阿里大鱼“管理中心-号码管理”中申请或购买的号码
            'calledShowNum'     => null,
        ],
        /*
         * -----------------------------------
         * SendCloud
         * -----------------------------------
         * website: http://sendcloud.sohu.com/sms/
         * support template sms.
         */
        'SendCloud' => [
            'smsUser'   => 'your_SMS_USER',
            'smsKey'    => 'your_SMS_KEY',
        ],
        /*
         * -----------------------------------
         * SmsBao
         * -----------------------------------
         * website: http://www.smsbao.com
         * support content sms.
         */
        'SmsBao' => [
            //注册账号
            'username'  => 'your_username',
            //账号密码（明文）
            'password'  => 'your_password',
        ],

        /*
        * -----------------------------------
        * Qcloud
        * 腾讯云
        * -----------------------------------
        * website:http://www.qcloud.com
        * support template sms.
        */
        'Qcloud' => [
            'appId'     => 'your_app_id',
            'appKey'    => 'your_app_key',
        ],
        /*
         * -----------------------------------
         * Aliyun
         * 阿里云
         * -----------------------------------
         * website:https://www.aliyun.com/product/sms
         * support template sms.
         */
        'Aliyun' => [
            'accessKeyId'       => 'LTAI2I1qC1UeyIOE',
            'accessKeySecret'   => '0KOJR8UNa6YyXWvda2wnfhZ3qNbvmD',
            'signName'          => '云微商',
            'regionId'          => 'cn-zhengzhou',
        ],

    ]

];